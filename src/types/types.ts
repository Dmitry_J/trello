import { Dispatch } from "react";

export interface hookOpenCardTypes {
    isOpenCardModal: boolean;
    setIsOpenCardModal: Dispatch<boolean>;
    openCard: OpenCardType;
    setOpenCard: Dispatch<OpenCardType>;
}

export interface OpenCardType {
    openCardId: number | null | undefined;
    columnId: string;
}