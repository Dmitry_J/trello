import { createAction } from "@reduxjs/toolkit";
import { TitleType } from "./types";

const TITLE: string = 'TITLE'

export const titleAction = createAction<TitleType>(TITLE);

export const columnActions = {
    titleAction
}