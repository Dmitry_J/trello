export interface TitleType {
    title: string;
    id: string
}

export interface ActionType {
    type: string;
    payload: TitleType;
}

export interface InitialStateType {
    columnTitles: TitleType[]
}