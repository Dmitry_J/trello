import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../store";

export const selectColumnTitles = createSelector(
    (state: RootState) => state.titleColumn,
    (cards) => cards.columnTitles
)

export const columnSelectors = {
    selectColumnTitles
}