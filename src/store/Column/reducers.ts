import { createReducer } from "@reduxjs/toolkit";
import { ActionType, InitialStateType } from "./types";
import * as actions from './actions'

const initialState: InitialStateType = {
    columnTitles: [
        {
            id: '0',
            title: 'TODO'
        },
        {
            id: '1',
            title: 'In Progress'
        },
        {
            id: '2',
            title: 'Testing'
        },
        {
            id: '3',
            title: 'Done'
        }
    ]
}

export const columnReducer = createReducer(initialState, (builder) => {
    builder.addCase(actions.titleAction, (state, action: ActionType) => {
        state.columnTitles.forEach(item => {
            if (item.id === action.payload.id) {
                item.title = action.payload.title;
            }
        });
    })
})

export default columnReducer;
