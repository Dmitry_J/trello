import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../store";

export const selectCards = createSelector(
    (state: RootState) => state.cards,
    (cards) => cards.cards
)

export const selectOpenCards = createSelector(
    (state: RootState) => state.cards,
    (cards) => cards.openCard
)

export const selectCommentList = createSelector(
    (state: RootState) => state.cards,
    (cards) => cards.commentList
)

export const cardSelectors = {
    selectCards,
    selectOpenCards,
    selectCommentList
}