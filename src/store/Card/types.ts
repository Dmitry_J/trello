export interface StateAction {
    cards: CardItemType[];
    commentList?: Comment[];
    openCard?: boolean;
}

export interface CardItemType {
    id: string;
    cardList: CardListType[];
}

export interface CardListType {
    desc?: string;
    comments?: CommentType[];
    comment?: CommentType;
    id?: number| null; //поправить
    title?: string; 
    titleId?: string | null; //поправить
    arrayIndex?: number;
    openCard?: boolean;
    columnTitle?: string;
}

export interface Comment {
    cardId: number | null | undefined;
    comment: CommentType;
}

export interface CommentType {
    id: number | null;
    comment?: string;
}
