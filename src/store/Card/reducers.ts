import { createReducer } from "@reduxjs/toolkit";
import { StateAction } from "./types";
import * as actions from './actions';

const initialState: StateAction = {
    cards: [
        {
            id: '0',
            cardList: []
        },
        {
            id: '1',
            cardList: []
        },
        {
            id: '2',
            cardList: []
        },
        {
            id: '3',
            cardList: []
        }
    ],
    commentList: [],
}

export const cardReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(actions.cardAddAction, (state: StateAction, action) => {
            state.cards.forEach(card => {
                if (card.id === action.payload.titleId) {
                    const newCard = {
                        title: action.payload.title,
                        id: action.payload.id
                    }
                    card.cardList.push(newCard);
                }
            })
        })
        .addCase(actions.cardDeleteAction, (state, action) => {
            state.cards.forEach(card => {
                if (card.id === action.payload.titleId && action.payload.arrayIndex !== undefined) {
                    card.cardList.splice(action.payload.arrayIndex, 1);
                }
            })
        })
        .addCase(actions.cardTitleAction, (state, action) => {
            state.cards.forEach(card => {
                if (card.id === action.payload.titleId) {
                    card.cardList.forEach(item => {
                        if (item.id === action.payload.id) {
                            item.title = action.payload.title;
                        }
                    })
                }
            })
        })
        .addCase(actions.cardSetDescAction, (state, action) => {
            state.cards.forEach(card => {
                if (card.id === action.payload.titleId) {
                    card.cardList.forEach(item => {
                        if (item.id === action.payload.id) {
                            item.desc = action.payload.desc;
                        }
                    })
                }
            })
        })
        .addCase(actions.cardAddCommentAction, (state, action) => {
           state.commentList?.push(action.payload);
        })
        .addCase(actions.cardEditCommentAction, (state, action) => {
            state.commentList?.forEach(item => {
                if (item.comment.id === action.payload.id) {
                    item.comment.comment = action.payload.comment
                }
            })
        })
        .addCase(actions.cardDeleteCommentAction, (state, action) => {
            state.commentList = state.commentList?.filter(item => {
                return item.comment.id !== action.payload.id
            })
        })
})

export default cardReducer;
