import { createAction } from "@reduxjs/toolkit";
import { CardListType, Comment, CommentType } from "./types";

const ADD_CARD: string = 'ADD_CARD';
const DELETE_CARD: string = 'DELETE_CARD';
const TITLE_CARD: string = 'TITLE_CARD';
const SET_DESC_CARD: string = 'SET_DESC_CARD';
const ADD_COMMENT_CARD: string = 'ADD_COMMENT_CARD';
const EDIT_COMMENT_CARD: string = 'EDIT_COMMENT_CARD';
const DELETE_COMMENT_CARD: string = 'DELETE_COMMENT_CARD';

export const cardAddAction = createAction<CardListType>(ADD_CARD);
export const cardDeleteAction = createAction<CardListType>(DELETE_CARD);
export const cardTitleAction = createAction<CardListType>(TITLE_CARD);
export const cardSetDescAction = createAction<CardListType>(SET_DESC_CARD);
export const cardAddCommentAction = createAction<Comment>(ADD_COMMENT_CARD);
export const cardEditCommentAction = createAction<CommentType>(EDIT_COMMENT_CARD);
export const cardDeleteCommentAction = createAction<CommentType>(DELETE_COMMENT_CARD);

export const cardActions = {
    cardAddAction,
    cardDeleteAction,
    cardTitleAction,
    cardSetDescAction,
    cardAddCommentAction,
    cardEditCommentAction,
    cardDeleteCommentAction
}