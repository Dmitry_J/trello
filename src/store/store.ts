import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import {columnReducer} from "./Column";
import {cardReducer} from "./Card";
import {registrationReducer} from "./Registration";

const persistConfig = {
    key: 'root',
    storage,
}

const reducer = combineReducers({
    titleColumn: columnReducer,
    cards: cardReducer,
    name: registrationReducer
})

export const store = configureStore({
    reducer: persistReducer(persistConfig, reducer),
    middleware: []
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;