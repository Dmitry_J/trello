import { registrationSelectors } from "./Registration/selectors";
import { registrationActions } from "./Registration/actions";

import { columnSelectors } from "./Column/selectors";
import { columnActions } from "./Column/actions";

import { cardActions } from "./Card/actions";
import { cardSelectors } from "./Card/selectors";

import reducer from './store';

const actions = {
    user: registrationActions,
    column: columnActions,
    card: cardActions
}

const selectors = {
    user: registrationSelectors,
    column: columnSelectors,
    card: cardSelectors
}

export {
    actions,
    selectors,
    reducer
}