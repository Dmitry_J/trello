import { createReducer } from "@reduxjs/toolkit";
import { StateActionType } from "./types";
import * as actions from './actions'

const initialState: StateActionType = {
    regName: '',
}

export const registrationReducer = createReducer(initialState, (builder) => {
    builder.addCase(actions.addNameAction, (state, action) => {
        state.regName = action.payload.regName;
    })
})

export default registrationReducer;
