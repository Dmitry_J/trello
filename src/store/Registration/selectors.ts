import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../store";

export const selectRegName = createSelector(
    (state: RootState) => state.name,
    (cards) => cards.regName
)

export const registrationSelectors = {
    selectRegName
}