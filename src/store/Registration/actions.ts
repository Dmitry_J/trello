import { createAction } from "@reduxjs/toolkit";
import { StateActionType } from "./types";

const NAME: string = 'NAME';

export const addNameAction = createAction<StateActionType>(NAME);

export const registrationActions = {
    addNameAction
}