import React, { useState, useRef } from "react";
import { Title } from "../../ui/Title";
import { Button } from "../../ui/Button";
import { CardList } from "./components/CardList";
import { Input } from "../../ui/Input";

import { actions } from "../../store/ducks";
import { selectors } from "../../store/ducks";

import { cardTypes } from "../../store/Card";
import { columnTypes } from "../../store/Column";
import { useAppSelector, useAppDispatch } from "../../store/hooks";
import * as Types from '../../types/types';
import { Form, Field } from 'react-final-form';

import styled from "styled-components";


const Column: React.FC<ColumnProps> = ({ id, hookOpenCard }) => {

    const cards = useAppSelector(selectors.card.selectCards);
    const columnTitle = useAppSelector(selectors.column.selectColumnTitles);
    const dispatch = useAppDispatch();
    const [isOpenTitleEdit, setIsOpenTitleEdit] = useState<boolean>(false);
    const titleRef = useRef<HTMLInputElement>(null);
    const titleCardRef = useRef<HTMLInputElement>(null);

    let title: string|undefined = '';

    columnTitle.forEach((item: columnTypes.TitleType) => {
        if (item.id === id) {
            title = item.title;
        }
    })

    const changeTitle = (event: React.KeyboardEvent<HTMLInputElement>): void => {
        if (event.key === 'Enter') {
            edit();
            setIsOpenTitleEdit(false);
        }
    }

    const focusHandler = (event: React.FocusEvent<HTMLInputElement>): void => edit();

    const editTitle = (event: React.MouseEvent<HTMLButtonElement>): void => edit();

    function edit() {
        const newTitle: columnTypes.TitleType = {
            title: titleRef.current!.value,
            id: id
        }

        dispatch(actions.column.titleAction(newTitle));
        setIsOpenTitleEdit(false);
    }

    const addCard = (value: VauleInput) => {
        if (value.addCard === '') return;
        if (value.addCard !== undefined) {
            const cardItem: cardTypes.CardListType = {
                title: value.addCard,
                id: Date.now(),
                titleId: id
            }
            cards.forEach((item: cardTypes.CardItemType) => {
                if (item.id === id) {
                    dispatch(actions.card.cardAddAction(cardItem))
                }
            })
            value.addCard = '';
        }
    }

    const getText = (event: React.MouseEvent) => setIsOpenTitleEdit(true);

    return (
        <ColumnWrapper>
            <Form
                onSubmit={addCard}
                render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    {!isOpenTitleEdit ? 
                        <Title 
                            text={title} 
                            onClick={getText}
                            containerStyles="font-size: 20px; text-align: start; cursor: pointer; min-height: 23px"
                        /> 
                        : 
                        <>
                            <Input 
                                defaultValue={title} 
                                inputRefs={titleRef} 
                                onBlur={focusHandler} 
                                onKeyPress={changeTitle}
                                containerStyles="background-color: #fff;
                                    border-radius: 3px;
                                    box-shadow: none;
                                    border: none;
                                    width: 100%;
                                    font-size: 20px;
                                    font-weight: 600;
                                    height: 32px;
                                    line-height: 24px;
                                    min-height: 24px;
                                    padding: 4px 8px;
                                    resize: none;
                                    cursor: auto;"
                            />
                            
                            <Button text='edit title' onClick={editTitle}/>
                        </>
                    }
                    <CardList columnId={id} hookOpenCard={hookOpenCard}/>
                        <Field name="addCard">
                            {({input}) => (
                                <Input 
                                    {...input}
                                    placeholder="Enter the name of the card" 
                                    inputRefs={titleCardRef}
                                    containerStyles="font-size: 16px;
                                        padding: 10px;
                                        border: none;
                                        margin-bottom: 15px"
                                />
                            )}
                        </Field>
                    <Button text="Add card" />
                </form>
                )}
            />
        </ColumnWrapper>
    );
}

interface ColumnProps {
    id: string;
    hookOpenCard: Types.hookOpenCardTypes;
}

interface VauleInput {
    addCard: string
}

const ColumnWrapper = styled.div`
    background-color: #f1f1f1;
    padding: 15px;
    box-shadow: 0px 5px 10px 2px rgb(34 60 80 / 20%);
    width: 300px;
    margin: 20px;
`;

export default Column;