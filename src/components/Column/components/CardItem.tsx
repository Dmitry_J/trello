import React, { useState, useRef } from "react";
import styled from "styled-components";
import { ButtonEdit } from "../../../ui/ButtonEdit";
import { Input } from "../../../ui/Input";
import { Button } from "../../../ui/Button";

import { actions } from "../../../store/ducks";
import { selectors } from "../../../store/ducks";

import { cardTypes } from "../../../store/Card";
import { useAppSelector, useAppDispatch } from "../../../store/hooks";
import * as Types from '../../../types/types'


export const CardItem: React.FC<CardItemProps> = ({ columnId, itemList, i, hookOpenCard }) => {

    const commentList = useAppSelector(selectors.card.selectCommentList);
    const [isOpenTitleEdit, setIsOpenTitleEdit] = useState<boolean>(false);
    const inputRef = useRef<HTMLInputElement>(null);
    const dispatch = useAppDispatch();

    let commentsCount: number = 0;

    commentList!.forEach(item => {
        if (item.cardId === itemList.id) {
            commentsCount++
        }
    })

    const deleteCard = (i: number) => {
        const cardItem: cardTypes.CardListType = {
            titleId: columnId,
            arrayIndex: i
        }
        dispatch(actions.card.cardDeleteAction(cardItem)); 
    }

    const openEditTitleCard = (i: number) => setIsOpenTitleEdit(true);

    const editTitle = (event: React.KeyboardEvent): void => {
        if (event.key === 'Enter') {
            const cardItem: cardTypes.CardListType = {
                titleId: columnId,
                id: itemList.id,
                title: inputRef.current?.value
            }
            dispatch(actions.card.cardTitleAction(cardItem))
            setIsOpenTitleEdit(false);
        }
    }

    const editTitleOnClick = (event: React.MouseEvent): void => {
        const cardItem: cardTypes.CardListType = {
            titleId: columnId,
            id: itemList.id,
            title: inputRef.current?.value
        }
        dispatch(actions.card.cardTitleAction(cardItem))
        setIsOpenTitleEdit(false);
    }

    const openCard = () => {
        
        const newOpenCard: Types.OpenCardType = {
            columnId: columnId,
            openCardId: itemList.id
        }
        hookOpenCard.setOpenCard(newOpenCard);
        hookOpenCard.setIsOpenCardModal(true);
    };

    return (
        <li>
            {!isOpenTitleEdit ? 
                <ListItemWrapper style={{position: 'relative'}}>
                    <ListItem  onClick={openCard}>
                        {itemList.title}
                        <div>Comments: {commentsCount}</div>
                    </ListItem>
                    <ButtonWrapper>
                        <ButtonEdit 
                            containerStyles="background-color: red; 
                                padding: 5px; 
                                cursor: pointer"
                            text='delete' 
                            onClick={() => {deleteCard(i)}}
                        />
                        <ButtonEdit 
                            text='edit title' 
                            containerStyles="padding: 5px; 
                                cursor: pointer" 
                            onClick={() => {openEditTitleCard(i)}} 
                        />
                    </ButtonWrapper>
                </ListItemWrapper> : 
                <>
                    <Input defaultValue={itemList.title} inputRefs={inputRef} onKeyPress={editTitle}/>
                    <Button text='edit title' onClick={editTitleOnClick}/>
                </>
            }
        </li> 
    )
}

interface CardItemProps {
    columnId: string;
    itemList: cardTypes.CardListType;
    i: number;
    hookOpenCard: Types.hookOpenCardTypes;
}

const ListItemWrapper = styled.div`
    position: relative;
`;

const ListItem = styled.div`
    padding: 5px;
    padding-right: 125px;
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 0 1px 0 rgb(9 30 66 / 25%);
    cursor: pointer;
    display: block;
    margin-bottom: 8px;
    max-width: 300px;
    min-height: 20px;
    position: relative;
    text-decoration: none;
    z-index: 0;
`;

const ButtonWrapper = styled.div`
    display: flex;
    position: absolute;
    top: 0;
    right: 0;
    height: 100%;
`;