import React from "react";
import styled from "styled-components";
import { CardItem } from "./CardItem";
import { selectors } from "../../../store/ducks";
import { cardTypes } from "../../../store/Card";
import { useAppSelector } from "../../../store/hooks";
import * as Types from '../../../types/types';

let cardsInColumn: cardTypes.CardListType[] = [];

export const CardList: React.FC<CardProps> = ({ columnId, hookOpenCard }) => {

    const cards = useAppSelector(selectors.card.selectCards);

    cards.forEach((item: cardTypes.CardItemType) => {
        if (item.id === columnId) {
            cardsInColumn = item.cardList;
        }
    })

    return (
        <List>
            {cardsInColumn.map((card: cardTypes.CardListType, i: number) => <CardItem 
                key={card.id} 
                itemList={card} 
                i={i} 
                columnId={columnId}
                hookOpenCard={hookOpenCard}
            />)}
        </List>
    );
}

interface CardProps {
    columnId: string;
    hookOpenCard: Types.hookOpenCardTypes,
}

const List = styled.ul`
    padding: 0;
    list-style-type: none;
`;