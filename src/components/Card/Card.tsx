import React, { useEffect, useRef, useState } from "react";
import { CloseButton } from "../../ui/CloseButton";
import { CommentList } from "../CommentList";
import { Button } from "../../ui/Button";
import { Title } from "../../ui/Title";
import { TextArea } from "../../ui/TextArea";
import styled from "styled-components";

import { actions } from "../../store/ducks";
import { selectors } from "../../store/ducks";

import { cardTypes } from "../../store/Card";
import { useAppSelector, useAppDispatch } from "../../store/hooks";
import * as Types from '../../types/types';

const Card: React.FC<CardType> = ({ hookOpenCard }) => {

    const regName = useAppSelector(selectors.user.selectRegName);
    const cards = useAppSelector(selectors.card.selectCards);
    const columnTitle = useAppSelector(selectors.column.selectColumnTitles);
    const dispatch = useAppDispatch();
    const [isOpenDescEdit, setIsOpenDescEdit] = useState<boolean>(false);
    const textRef = useRef<HTMLTextAreaElement>(null);
    const commentRef = useRef<HTMLTextAreaElement>(null);

    let columnTitleCard: string = ''
    let desc: string = 'add desc';
    let titleCard: string | undefined = '';

    cards.forEach((cards: cardTypes.CardItemType) => {
        if (cards.id === hookOpenCard.openCard.columnId) {
            cards.cardList.forEach((item: cardTypes.CardListType) => {
                if(item.id === hookOpenCard.openCard.openCardId && item.desc) {
                    desc = item.desc;
                }
                if(item.id === hookOpenCard.openCard.openCardId) {
                    titleCard = item.title;
                }
            })
        }
    })

    columnTitle.forEach(column => {
        if (column.id === hookOpenCard.openCard.columnId) {
            columnTitleCard = column.title
        }
    })

    const closePopup = (event: React.MouseEvent) => hookOpenCard.setIsOpenCardModal(false)

    const addDescription = (event: React.FocusEvent) => {

        dispatch(actions.card.cardSetDescAction({
            titleId: hookOpenCard.openCard.columnId,
            id: hookOpenCard.openCard.openCardId,
            desc: textRef.current?.value
        }));

        setIsOpenDescEdit(false);
    }

    const openTextEdit = () => setIsOpenDescEdit(true);

    const addComment = (event: React.MouseEvent) => {
        event.preventDefault();

        if (commentRef.current?.value !== undefined) {
            const newComment: cardTypes.CommentType = {
                id: Date.now(),
                comment: commentRef.current?.value
            }

            const comment: cardTypes.Comment = {
                cardId: hookOpenCard.openCard.openCardId,
                comment: newComment
            }
            
            dispatch(actions.card.cardAddCommentAction(comment))

            commentRef.current.value = '';
        }
    }

    const addDesc = (event: React.MouseEvent) => {
        event.preventDefault();

        dispatch(actions.card.cardSetDescAction({
            titleId: hookOpenCard.openCard.columnId,
            id: hookOpenCard.openCard.openCardId,
            desc: textRef.current?.value
        }));

        setIsOpenDescEdit(false);
    }

    useEffect(() => {
        document.addEventListener('keydown', (e) => {
            if (e.key === 'Escape') {
                hookOpenCard.setIsOpenCardModal(false);
            }
        });
    });

    return (
        <Overlay>
            <CardModal>
                <CloseButton onClick={closePopup}/>
                    <Wrapper style={{margin: '0'}}>
                        <Title 
                            text={'Author card ' + regName} 
                            containerStyles={TitleStyles}
                        />
                    </Wrapper>
                    <Wrapper>
                        <Title text={titleCard} containerStyles={TitleStyles}/>
                        <SubTitle>in column <SubTitleSpan>{columnTitleCard}</SubTitleSpan></SubTitle>
                    </Wrapper>
                    <Wrapper>
                        <Title text='Card desc' containerStyles={TitleStyles}/>
                        {!isOpenDescEdit ? 
                            <TextDesc onClick={openTextEdit}>{desc || 'add desc'}</TextDesc> :
                            <>
                            <TextArea
                                placeholder="add desc" 
                                onBlur={addDescription}
                                defaultValue={desc}
                                textAreaRef={textRef}
                            />
                            <Button text='Add desc' onClick={addDesc}/>
                        </>}
                    </Wrapper>
                    <Wrapper>
                        <Title text='Card comments' containerStyles={TitleStyles}/>
                        <CommentList openCardId={hookOpenCard.openCard.openCardId}/>
                        <TextArea 
                            placeholder="add comment" 
                            textAreaRef={commentRef}
                        />
                        <Button text='Add comment' onClick={addComment} />
                    </Wrapper>
            </CardModal>
        </Overlay>
    )
}

interface CardType {
    hookOpenCard: Types.hookOpenCardTypes;
}

const TitleStyles: string = "font-size: 24px; text-align: start";

const CardModal = styled.div`
    padding: 20px;
    position: absolute;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fff;
    width: 1000px;
    z-index: 20;
    max-height: calc(100% - 10vh);
    overflow: auto;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 30px;
`;

const SubTitle = styled.p`
    margin: 0;
    color: #5e6c84;
`;

const SubTitleSpan = styled.span`
    border-bottom: 1px solid;
`;

const TextDesc = styled.div`
    padding: 5px;
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 1px 1px 10px rgb(9 30 66 / 25%);
    cursor: pointer;
    display: block;
    margin-bottom: 8px;
    min-height: 20px;
    position: relative;
    -webkit-text-decoration: none;
    text-decoration: none;
    z-index: 0;
`;

const Overlay = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 10;
`;

export default Card;