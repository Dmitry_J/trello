import React, { useState, useRef } from "react";
import styled from "styled-components";
import { Title } from "../../../ui/Title";
import { ButtonEdit } from "../../../ui/ButtonEdit";
import { TextArea } from "../../../ui/TextArea";
import { Button } from "../../../ui/Button";

import { selectors } from "../../../store/ducks";
import { actions } from "../../../store/ducks";

import { cardTypes } from "../../../store/Card";
import { useAppSelector, useAppDispatch } from "../../../store/hooks";


export const CommentItem: React.FC<CommentItemProps> = ({ itemComment, indexElement, commentId }) => {

    const regName = useAppSelector(selectors.user.selectRegName);
    const dispatch = useAppDispatch();
    const [isOpenCommentEdit, setIsOpenCommentEdit] = useState<boolean>(false);
    const commentRef = useRef<HTMLTextAreaElement>(null);

    const authorComment: string = `Author comment ${regName}`;


    const openEditComment = (): void => setIsOpenCommentEdit(true);

    const editComment = (event: React.FocusEvent): void => edit();

    const editCommentToClick = (event: React.MouseEvent): void => edit();

    function edit() {
        if (commentRef.current?.value !== undefined) {
            dispatch(actions.card.cardEditCommentAction({
                id: commentId,
                comment: commentRef.current?.value
            }))

            setIsOpenCommentEdit(false);
        }

    }

    const deleteComment = (indexElement: number) => {
        dispatch(actions.card.cardDeleteCommentAction({
            id: commentId,
        }));
    }

    return (
        <>
            {!isOpenCommentEdit ? 
                <Comment key={itemComment.id}>
                    <div>{itemComment.comment}</div>
                    <Title 
                        text={authorComment} 
                        containerStyles="margin: 5px;
                            text-align: start;
                            font-size: 20px"
                    />
                    <ButtonWrapper>
                        <ButtonEdit 
                            text='edit comment' 
                            containerStyles="padding: 5px;
                                cursor: pointer;"
                            onClick={openEditComment}
                        />
                        <ButtonEdit 
                            text='delete comment' 
                            containerStyles="background-color: red;
                                padding: 5px;
                                cursor: pointer;"
                            onClick={() => deleteComment(indexElement)}
                        />
                    </ButtonWrapper>
                </Comment>
            :
            <>
                <TextArea defaultValue={itemComment.comment} onBlur={editComment} textAreaRef={commentRef}/>
                <Button text='edit comment' onClick={editCommentToClick}/>
            </>
            }
        </>
    );
}

interface CommentItemProps {
    itemComment: cardTypes.CommentType;
    key: number | null;
    indexElement: number;
    commentId: number | null;
}

const ButtonWrapper = styled.div`
    display: flex;
    position: absolute;
    top: 0;
    right: 0;
    height: 100%;
`;

const Comment = styled.li`
    padding: 5px;
    padding-right: 220px;
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 1px 1px 10px rgb(9 30 66 / 25%);
    cursor: pointer;
    display: block;
    margin-bottom: 8px;
    min-height: 20px;
    position: relative;
    text-decoration: none;
    z-index: 0;
`;