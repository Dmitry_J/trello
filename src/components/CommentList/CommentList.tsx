import React from "react";
import { CommentItem } from "./components/CommentItem";
import { selectors } from "../../store/ducks";
import { cardTypes } from "../../store/Card";
import { useAppSelector } from "../../store/hooks";

const CommentList: React.FC<CommentListProps> = ({openCardId}) => {

    const commentList = useAppSelector(selectors.card.selectCommentList);

    let comments: cardTypes.CommentType[] = [];

    commentList!.forEach(item => {
        if (item.cardId === openCardId) {
            comments.push(item.comment);
        }
    });

    return (
        <ul>
            {comments.map((item: cardTypes.CommentType, i: number) => 
                <CommentItem 
                    key={item.id} 
                    commentId={item.id} 
                    itemComment={item} 
                    indexElement={i}
                />
            )}
        </ul>
    );
}

interface CommentListProps {
    openCardId: number | null | undefined;
}

export default CommentList;