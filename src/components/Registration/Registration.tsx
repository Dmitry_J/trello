import React from 'react';
import { Button } from '../../ui/Button';
import { Title } from '../../ui/Title';
import { Input } from '../../ui/Input';
import styled from 'styled-components';
import { useAppDispatch } from '../../store/hooks';
import { actions } from '../../store/ducks';

import { Form, Field, FormProps } from 'react-final-form';

const Registration: React.FC = () => {

    const dispatch = useAppDispatch();

    const getName = (values: FormProps) => dispatch(actions.user.addNameAction({regName: values.username}));

    return (
        <Overlay>
            <Modal>
                <Form
                    onSubmit={getName}
                    render={({ handleSubmit }) => (
                    <form onSubmit={handleSubmit}>
                        <Title text="Enter your name"/>
                        <Field name="username">
                            {({input}) => (
                                <Input {...input}/>
                            )}
                        </Field>
                        <Button text="Ok"/>
                    </form>
                    )}
                />
            </Modal>
        </Overlay>
    )
}

const Overlay = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 10;
`;

const Modal = styled.div`
    padding: 20px;
    position: absolute;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #fff;
    width: 500px;
    z-index: 20;
`;

export default Registration;