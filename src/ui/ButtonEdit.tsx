import React from "react";
import styled, { CSSProp } from "styled-components";

export const ButtonEdit = ({ text, containerStyles = {}, ...outerProps }: ButtonEditProps): JSX.Element => {   
    return (
        <Root 
            $CSS={containerStyles}
            {...outerProps}
        >
            {text}
        </Root>
    )
}

interface ButtonEditProps extends React.HTMLAttributes<HTMLDivElement> {
    text: string;
    containerStyles?: CSSProp;
}

type ButtonRoot = {
    $CSS?: CSSProp
}

const Root = styled.div<ButtonRoot>`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 5px;
    cursor: pointer;
    ${({$CSS}) => $CSS}
`;