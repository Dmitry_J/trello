import React from "react";
import styled, { CSSProp } from "styled-components";

export const Title = ({ text, containerStyles = {}, ...outerProps }: TitleProps ): JSX.Element => {
    return (
        <TitleElement 
            $CSS={containerStyles}
            {...outerProps}
        >
            {text}
        </TitleElement>
    );
}

interface TitleProps extends React.HTMLAttributes<HTMLHeadingElement> {
    text: string | undefined;
    containerStyles?: CSSProp;
}

type TitleRoot = {
    $CSS?: CSSProp
}

const TitleElement = styled.h1<TitleRoot>`
    text-align: center;
    ${({$CSS}) => $CSS}
`;