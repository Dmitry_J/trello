import React, { ButtonHTMLAttributes } from "react";
import styled, { CSSProp } from "styled-components";

export const Button = ({ text, containerStyled = {}, ...button}: ButtonProps): JSX.Element => {
    return (
        <Root 
            $CSS={containerStyled} 
            {...button}
        >
            {text}
        </Root>
    );
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    text: string;
    containerStyled?: CSSProp
}

type ButtonRoot = {
    $CSS?: CSSProp
}


const Root = styled.button<ButtonRoot>`
    width: 100%;
    cursor: pointer;
    font-size: 16px;
    padding: 10px;
    background-color: #bbdae9;
    border: none;
    color: #0079bf;
    ${({$CSS}) => $CSS}
`;