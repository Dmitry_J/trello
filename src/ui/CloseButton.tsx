import React from "react";
import styled from "styled-components";

export const CloseButton = ({ ...button }: React.HTMLAttributes<HTMLDivElement>): JSX.Element => {
    return (
        <Root {...button}/>
    )
}


const Root = styled.div`
    position: absolute;
    top: 15px;
    right: 15px;
    height: 50px;
    width: 50px;
    border-radius: 50%;
    cursor: pointer;
    transition: 0.25s;
    z-index: 20;
    &::before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(45deg);
        width: 50%;
        height: 2px;
        background-color: black;
    };
    &::after {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(-45deg);
        width: 50%;
        height: 2px;
        background-color: black;
    };
    &:hover {
        background-color: rgba(9,30,66,.08);
    }
`;