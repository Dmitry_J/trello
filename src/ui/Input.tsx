import React, { InputHTMLAttributes, MutableRefObject } from "react";
import styled, { CSSProp } from "styled-components";

export const Input = ({ inputRefs, containerStyles = {}, ...input}: InputProps): JSX.Element => {

    return (
        <Root 
            ref={inputRefs} 
            $CSS={containerStyles}
            {...input}
        />
    );
}

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    inputRefs?: MutableRefObject<HTMLInputElement | null>;
    containerStyles?: CSSProp;
}

type InputRoot = {
    $CSS?: CSSProp
}

const Root = styled.input<InputRoot>`
    width: 100%;
    height: 30px;
    margin-top: 20px;
    margin-bottom: 20px;
    ${({$CSS}) => $CSS}
`;