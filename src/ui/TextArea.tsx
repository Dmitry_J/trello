import React, { TextareaHTMLAttributes, MutableRefObject } from "react";
import styled, { CSSProp } from "styled-components";

export const TextArea = ({ textAreaRef, containerStyles = {}, ...textarea }: TextAreaProps): JSX.Element => {
    return (
        <Root  
            ref={textAreaRef} 
            $CSS={containerStyles}
            {...textarea}
        />
    );
}

interface TextAreaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
    textAreaRef: MutableRefObject<HTMLTextAreaElement | null>;
    containerStyles?: CSSProp
}

type TextAreaRoot = {
    $CSS?: CSSProp
}

const Root = styled.textarea<TextAreaRoot>`
    margin: 0px;
    height: 64px;
    width: 100%;
    background-color: #e4f0f6;
    box-shadow: none;
    border: none;
    color: #0079bf;
    outline: 0;
    resize: none;
    ${({$CSS}) => $CSS}
`;