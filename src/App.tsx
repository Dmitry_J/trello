import React, { useState } from 'react';
import styled from 'styled-components';
import { Registration } from './components/Registration';
import { Column } from './components/Column';
import { Card } from './components/Card';
import { useAppSelector } from './store/hooks';
import { registrationSelectors } from './store/Registration';
import * as Types from './types/types';
import './App.css';

const App: React.FC = () => {

    const [isOpenCardModal, setIsOpenCardModal] = useState<boolean>(false);
    const [openCard, setOpenCard] = useState<Types.OpenCardType>({
        columnId: '',
        openCardId: null
    })
    const regName = useAppSelector(registrationSelectors.selectRegName);

    const hookOpenCard: Types.hookOpenCardTypes = {
        isOpenCardModal, 
        setIsOpenCardModal,
        openCard,
        setOpenCard
    }

    return (
        <>
            {!regName && <Registration />}
            {isOpenCardModal && <Card hookOpenCard={hookOpenCard}/>}
            <Wrapper>
                <Column id="0" hookOpenCard={hookOpenCard}/>
                <Column id="1" hookOpenCard={hookOpenCard}/>
                <Column id="2" hookOpenCard={hookOpenCard}/>
                <Column id="3" hookOpenCard={hookOpenCard}/>
            </Wrapper>
        </>
    );
}

const Wrapper = styled.div`
    display: flex;
    align-items: start;
`;

export default App;